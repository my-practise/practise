package kz.aitu.oop.practice.practice4;



public enum Aquarium_inhabitants {
    Fish("Fish"),
    Reptiles("Reptiles"),
    Amphibians("Amphibians"),
    Molluscs("Molluscs"),
    Crustaceans("Crustaceans");


    private String type;


    Aquarium_inhabitants(String type){
        setType(type);
    }

    public void setType(String type) {
        this.type=type;
    }

    public String getType() {
        return type;
    }
}
