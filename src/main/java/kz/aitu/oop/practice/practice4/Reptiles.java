package kz.aitu.oop.practice.practice4;

public class Reptiles extends Aquarium_creature{
    private Detachment detachment;
    private  int number_of_limbs;
    private String nutrition;
   private int walking_speed;
   private static int count=0;

    public Reptiles(Aquarium_inhabitants type, String species_name, String name, int age, int lifespan,
                    String breeding_method, String swimming_speed,Detachment detachment, int number_of_limbs, String nutrition, int walking_speed) {
        super(type, species_name, name, age, lifespan, breeding_method, swimming_speed);
        setDetachment(detachment);
        setNumber_of_limbs(number_of_limbs);
        setNutrition(nutrition);
        setWalking_speed(walking_speed);
        count++;
    }

    public Detachment getDetachment() {
        return detachment;
    }

    public void setDetachment(Detachment detachment) {
        this.detachment = detachment;
    }

    public int getNumber_of_limbs() {
        return number_of_limbs;
    }

    public void setNumber_of_limbs(int number_of_limbs) {
        this.number_of_limbs = number_of_limbs;
    }

    public String getNutrition() {
        return nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }


    public int getWalking_speed() {
        return walking_speed;
    }

    public void setWalking_speed(int walking_speed) {
        this.walking_speed = walking_speed;
    }
}
