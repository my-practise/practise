package kz.aitu.oop.practice.practice4;

public enum Stirpes {
    Cichlids("Cichlids"),
    Carp ("Carp") ,
    Pecilia("Pecilia"),
    Haracin ("Haracin"),
    Aterinic ("Aterinic"),
    Pomacentral("Pomacentral"),
    Cartooth ("Pomacentral"),
    Labyrinth ("Labyrinth"),
    Iris ("Iris"),
    Catfish("Catfish"),
    Sturgeon("Sturgeon");

    private String stirpes;

    Stirpes(String stirpes) {
        this.stirpes = stirpes;
    }

    public String getStirpes() {
        return stirpes;
    }

    public void setStirpes(String stirpes) {
        this.stirpes = stirpes;
    }
}
