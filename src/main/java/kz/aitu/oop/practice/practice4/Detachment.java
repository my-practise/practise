package kz.aitu.oop.practice.practice4;

public enum Detachment {
    Scaly("Scaly"),
    Turtles("Turtles"),
    Crocodiles("Crocodiles"),
    Beakheads("Beakheads");

    private String detachment;

   Detachment(String detachment){
       setDetachment(detachment);
    }
    public String getDetachment() {
        return detachment;
    }

    public void setDetachment(String detachment) {
        this.detachment = detachment;
    }
}
