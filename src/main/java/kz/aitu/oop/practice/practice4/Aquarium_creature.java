package kz.aitu.oop.practice.practice4;

/**
 *
 */
public class Aquarium_creature {
    private Aquarium_inhabitants type;
    private String species_name;
    private String name;
    private int age;
    private int lifespan;
    private String breeding_method;
    private String swimming_speed;
    private  static int id=0;



    public Aquarium_creature() {

    }

    public Aquarium_creature( Aquarium_inhabitants type, String species_name,String name,int age, int lifespan, String breeding_method,String swimming_speed){
        setType(type);
        setSpecies_name(species_name);
        setName(name);
        setAge(age);
        setLifespan(lifespan);
        setBreeding_method(breeding_method);
        setSpeed(swimming_speed);
       id++;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies_name() {
        return species_name;
    }

    public void setSpecies_name(String species_name) {
        this.species_name = species_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLifespan() {
        return lifespan;
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
    }

    public Aquarium_inhabitants getType() {
        return type;
    }

    public void setType(Aquarium_inhabitants type) {
        this.type = type;
    }
    public static int getId() {
        return id;
    }


    public String getBreeding_method() {
        return breeding_method;
    }

    public void setBreeding_method(String breeding_method) {
        this.breeding_method = breeding_method;
    }

    public String getSpeed() {
        return swimming_speed;
    }

    public void setSpeed(String speed) {
        this.swimming_speed = speed;
    }
}
