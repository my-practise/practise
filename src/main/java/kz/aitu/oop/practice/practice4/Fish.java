package kz.aitu.oop.practice.practice4;

public class Fish extends Aquarium_creature {
       private Stirpes stirpes;
       private  String food;

    public Fish(Aquarium_inhabitants type, String species_name, String name, int age, int lifespan,
                String breeding_method, String swimming_speed,Stirpes stirpes,String food) {
        super(type, species_name, name, age, lifespan, breeding_method, swimming_speed);
        setStirpes(stirpes);
        setFood(food);
    }

    public Stirpes getStirpes() {
        return stirpes;
    }

    public void setStirpes(Stirpes stirpes) {
        this.stirpes = stirpes;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }
}
