package main.KZ.examples.task2;

public class MyString {
    private int[] values;
    private static int length;
    // keep the array values internally
    public MyString(int[] values) {
        this.values = values;
       length=values.length;
    }
    // return the number of values that are stored
    public int length(){
        return length;
        }

        public int valueAt(int position){
        if(position>length || position<values.length){
            return -1;
        }else{
            return values[position];
        }
        }

    public boolean contains(int value){
        for(int i=0; i<length; i++){
            if(value==values[i]){
                return true;

            }
        }
        return false;
    }

    public int count(int value){
        int counter=0;
        for(int i=0; i<length; i++){
            if(value==values[i]) {
                counter++;
            }
        }
        return counter;
    }

    public void print() {
        for(int i=0; i<length; i++){
            System.out.println(values[i]);
        }
    }
}
