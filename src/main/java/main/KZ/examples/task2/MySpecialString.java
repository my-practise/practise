package main.KZ.examples.task2;

public class MySpecialString {
    private int[] values;
    private static int leng;

    public MySpecialString(int[] values){

        for(int i=0; i<values.length; i++){
            for(int j=0; j<values.length; j++)
            if(values[i] == values[j]) {
                break;
            }
        }
        this.values=values;
        this.leng=values.length;
    }

    public int valueAt(int position){
        if(position>leng || position<values.length){
            return -1;
        }else{
            return values[position];
        }
    }
    public boolean contains(int value){
        for(int i=0; i<leng; i++){
            if(value==values[i]){
                return true;

            }
        }
        return false;
    }

    public int count(int value){
        int counter=0;
        for(int i=0; i<leng; i++){
            if(value==values[i]) {
                counter++;
            }
        }
        return counter;
    }

    public void print() {
        for(int i=0; i<leng; i++){
            System.out.println(values[i]);
        }
    }



}
