package main.KZ.examples.task2;

public class NewSpecialString {
    private String[] values;

    public NewSpecialString(String[] values){
        this.values=values;
    }
   public int length(){
        return values.length;
   }

    public String valueAt(int position){
        if(position>values.length || position<values.length){
            return "-1";
        }else{
            return values[position];
        }
    }
    public boolean contains(String value){
        for(int i=0; i<values.length; i++){
            if(value==values[i]){
                return true;

            }
        }
        return false;
    }

    public int count(String value){
        int counter=0;
        for(int i=0; i<values.length; i++){
            if(value==values[i]) {
                counter++;
            }
        }
        return counter;
    }

    public void print() {
        for(int i=0; i<values.length; i++){
            System.out.println(values[i]);
        }
    }



}
