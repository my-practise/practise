package week2.task2.domain.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {

        private Scanner sc = new Scanner(System.in);
        private User signedUser;
        private ArrayList<User> Userlist = new ArrayList<>();

        private void addUser(User user) {
            Userlist.add(user);
        }



        private void menu() {
            while (true) {
                if (signedUser == null) {
                    System.out.println("You are not signed in.");
                    System.out.println("1. Authentication");
                    System.out.println("2. Exit");
                    int choice = sc.nextInt();
                    if (choice == 1) authentication();
                    else break;
                }
                else {
                    try {
                        userProfile();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void userProfile() throws FileNotFoundException, UnsupportedEncodingException {
            while (true){
                System.out.println("1.Log off");
                int choice=sc.nextInt();
                if(choice==1){
                    logOff();
                }
            }


        }

        private void logOff() {
            signedUser = null;
        }

        private void authentication() {
            System.out.println("1.Sign in");
            System.out.println("2.Sign up");
            System.out.println("3.Back");
            int ch=sc.nextInt();
            if(ch==1){
                signIn();
            }else if(ch==2){
                signUp();
            }else if(ch==3){
                menu();
            }

        }

        private void signIn() {
            System.out.println("Enter your username:");
            String username=sc.nextLine();
            System.out.println("Enter your password:");
            String password=sc.nextLine();
            Password password1=new Password(password);

            for(User user : Userlist ) {
                if (username.equals(user.getUsername()) && password1.equals(user.getPassword())) {
                    signedUser = user;
                } else {
                    System.out.println("Error");
                }
            }

        }

        private void signUp() {
            User Current = new User();
            System.out.println("Enter your name:");
            String name=sc.nextLine();
            Current.setName(name);
            System.out.println("Enter your surname:");
            String surname=sc.nextLine();
            Current.setSurname(surname);
            System.out.println("Enter your username:");
            String username=sc.nextLine();
            if(!checkUsernamelist(username)){
                Current.setUsername(username);
            }else{
                System.out.println("This username used");
                System.out.println("1.Try again");
                System.out.println("2.Back menu");
                int ch1=sc.nextInt();
                if(ch1==1){
                    signUp();
                }else if(ch1==2){
                    menu();
                }
            }
            System.out.println("Enter password:");
            String password=sc.nextLine();
            Password password1=new Password(password);
            Current.setPassword(password1);
            addUser(Current);
        }
        private boolean checkUsernamelist(String username){
            for(User user:Userlist){
                if (username.equals(user.getUsername())){
                    return true;
                }
            }
            return false;
        }

        public void start() throws FileNotFoundException {
            File file = new File("C:\\Users\\auali\\OneDrive\\Рабочий стол\\Алмас\\Java\\db.txt");
            Scanner fileScanner = new Scanner(file);

            while (true) {
                System.out.println("Welcome to my application!");
                System.out.println("Select command:");
                System.out.println("1. Menu");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) {
                    menu();
                } else {
                    break;
                }
            }

            // save the userlist to db.txt
        }

        private void saveUserList() throws IOException {
            String con="";
            for (User user : Userlist){
                con+=user+"\n";
            }
            Files.write(Paths.get("C:\\Users\\auali\\OneDrive\\Рабочий стол\\Алмас\\Java\\db.txt"));
        }
    }


