package week2.task2.domain.domain;

public class User {
    private String Name;
    private String  Surname;
    private String Username;
    private int id;
    private static int id_gen=0;
    private Password password;

    public User(){
        generateid();
    }

    public void generateid(){
        id=id_gen++;

    }
    public User(int id,String Name,String Surname,String Username,String password){
        setId(id);
        setName(Name);
        setSurname(Surname);
        setUsername(Username);
        setPassword(new Password(password));

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        this.Surname = surname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        this.Username=username;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Password getPassword() {
        return password;
    }
}


