package week2.task2.domain.domain;

public class Password {
    private String password;

    public Password(String password) {
        setPassword(password);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if(CheckPassword(password)){
            this.password = password;
        }else{
            System.out.println("Incorrect password format.");

        }
    }

    public boolean CheckPassword(String password){
        if(password.length()<9){
            return false;
        }else{
            char p;
            int Up=0;
            int Low=0;
            int Dig=0;
            for (int i=0; i<password.length(); i++) {
                p = password.charAt(i);
                if (!Character.isLetterOrDigit(p)) {
                    if (p == '$' || p == '@' || p == '.' || p == ',' || p == '_' || p == '&' || p == '#') {
                        continue;
                    } else {
                        return false;
                    }
                } else if (Character.isDigit(p)) {
                    Dig++;
                } else if (Character.isUpperCase(p)) {
                    Up++;
                } else if (Character.isLowerCase(p)){
                    Low++;
                }
            }
            if(Up==0 || Low==0 || Dig==0){
                return false;
            }
        }
        return true;
    }

}
