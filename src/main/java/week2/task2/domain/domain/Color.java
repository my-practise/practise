package week2.task2.domain.domain;

public enum Color {
    blue("blue"),
    red("red"),
    white("white"),
    black("black"),
    pink("pink"),
    gray("gray"),
    green("green"),
    yellow("yellow");

    private String color;

    Color(String  color){
      setColor(color);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
