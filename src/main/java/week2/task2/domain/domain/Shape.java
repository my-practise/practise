package week2.task2.domain.domain;

public class Shape {
    private Color color;
    private boolean filled;

    public Shape(){
        setColor(Color.green);
        setFilled(true);
    }

    public Shape(Color color,boolean filled){
        setColor(color);
        setFilled(filled);
    }

    public void setColor(Color color){
        this.color = color;
    }

    public Color getColor(){
        return color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public boolean isFilled() {
        return filled;
    }

    @Override
    public String toString() {
        return "Shape[color = " + color + ", " + "filled = " + (filled ? ("filled"):("not filled")) + "]";
    }

}
