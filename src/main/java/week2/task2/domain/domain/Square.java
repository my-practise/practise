package week2.task2.domain.domain;

public class Square extends Rectangle {
    public Square(){}

    public Square(double side){
        super(side,side);
    }

    public Square(Color color, boolean filled, double side){
        super(color,filled,side,side);
    }

    public double getSide(){
        return getWidth();
    }

    public void setSide(double side){
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public void setLength(double length){
        setSide(length);
    }
    @Override
    public void setWidth(double width){
        setSide(width);
    }
    @Override
    public String toString() {
        return "Square[" + super.toString() +" "+ getSide()+"]";
    }

}
